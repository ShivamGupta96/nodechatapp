var socket = io.connect("http://localhost:6734");

var message = document.getElementById('message');
var username = document.getElementById('username');
var btn = document.getElementById('send');
var output = document.getElementById('output');

btn.addEventListener('click', () => {
    console.log("Event fired !!");
    socket.emit('chat', {
        message: message.value,
        username: username.value
    })
});

socket.on('chat', (data) => {
    output.innerHTML += "<p><strong>" + data.username + " : </strong>" + data.message + "</p>";
    // console.log("Handle : " + data.handle + " Message : " + data.message);
});