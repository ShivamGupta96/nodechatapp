var express = require('express');
var app = express();
var socket = require('socket.io');
var port = 6734;

var server = app.listen(port, () => {
    console.log("Server started on http://localhost:" + port);
});

app.use(express.static('public'));

var io = socket(server);

io.on('connection', (socket) => {
    console.log("made socket connection", socket.id);
    socket.on('chat', (data) => {
        io.sockets.emit('chat', data);
    });
});

